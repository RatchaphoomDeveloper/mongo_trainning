var express = require('express')
var router = express.Router()
var axios = require('axios')
var UserSchema = require("../models/UserSchema")
var CommentSchema = require("../models/CommentSchema")
var jwt = require("jsonwebtoken")
var Joi = require("joi")
var mongoose = require("mongoose")
const ObjectId = mongoose.Types.ObjectId;

const verify = require("./verifyToken")

const schema = Joi.object({
    name: Joi.string().min(6).required(),
    lastname: Joi.string().min(6).required(),
    email: Joi.string().min(6).email(),
    password: Joi.string().min(6).required()
})

router.get("/testRouter", async (req, res, next) => {
    res.status(200).json({ message: "running api ok" })
})

router.get("/insertComment", async (req, res, next) => {
    try {
        var comments = await axios.get(process.env.JSONPLACEHOLDER_WEBSERVICES + "/posts")
        if (comments) {
            var insComment = await CommentSchema.insertMany(comments.data).then(() => {
                console.log('data inserted')
            }).catch((error)=>{
                console.log(error)
            })
        }
        res.status(200).json({ message: "insert success",data:comments.data })
    } catch (error) {
        res.status(400).json({ message: error.message })
    }

})


router.get("/jsoncommnet",verify, async (req, res, next) => {
    try {
        var comments = await axios.get(process.env.JSONPLACEHOLDER_WEBSERVICES + "/comments")
        res.status(200).json({ message: "success", data: comments.data })
    } catch (error) {
        res.status(400).json({ message: error.message })
    }

})

router.post("/register", async (req, res, next) => {

    const { error } = schema.validate(req.body)
    if (error) return res.status(400).json(error)

    const userSchema = new UserSchema({
        name: req.body.name,
        lastname: req.body.lastname,
        email: req.body.email,
        password: req.body.password
    })
    try {
        const user = await userSchema.save();
        res.status(200).json({ message: "register success" })

    } catch (err) {
        res.status(400).json({ message: err.message })
    }
})

router.get("/getUsers", async (req, res, next) => {
    try {
        const users = await CommentSchema.find({})
        res.status(200).json({ message: "success", data: users })
    } catch (err) {
        res.status(400).json({ message: err.message })
    }
})

router.post("/getUser", async (req, res, next) => {
    try {
        const users = await UserSchema.findOne({ _id: req.body.id })
        res.status(200).json({ message: "success", data: users })
    } catch (err) {
        res.status(400).json({ message: err.message })
    }
})

router.post("/getUserAgg", async (req, res, next) => {
    try {

        // const useragg = await UserSchema.aggregate([
        //     {
        //         $match: { _id: ObjectId("60ceb3698c50631b98e3f0f0") }
        //     }
        // ])

        const useragg = await UserSchema.aggregate([
            {
                $match: { name: { $regex: `^${req.body.likename}` } } //like
            }
        ])
        // new mongoose.Types.ObjectId()
        res.status(200).json({ message: "success", data: useragg })

    } catch (err) {
        res.status(400).json({ message: err.message })
    }
})

router.post("/updateUser", async (req, res, next) => {
    try {

        const updateUser = await UserSchema.updateOne({ _id: req.body.id }, {
            name: "test12345",
            lastname: "lastnamenaja"
        })
        res.status(200).json({ message: "update success" })

    } catch (err) {
        res.status(400).json({ message: err.message })
    }
})

router.post("/deleteUser", async (req, res, next) => {
    try {
        const deleteUser = await UserSchema.deleteOne({ _id: req.body.id })
        res.status(200).json({ message: "delete success" })
    } catch (err) {
        res.status(400).json({ message: err.message })
    }

})


const loginSchema = Joi.object({
    email: Joi.string().min(6).email(),
    password: Joi.string().min(6).required()
})

router.post("/login", async (req, res, next) => {
    try {
        const { error } = loginSchema.validate(req.body)
        if (error) return res.status(400).json(error)
        const user = await UserSchema.findOne({ email: req.body.email, password: req.body.password })
        if (!user) return res.status(400).json({ message: "access denied" })
        const token = jwt.sign({ _id: user.id }, process.env.TOKEN_SECRET, { expiresIn: "10d" })
        res.header('auth-token', token)
        res.status(200).json({ message: "login success", data: user, token: token })
    } catch (err) {
        res.status(400).json({ message: err.message })
    }
})

router.post("/testVerify", verify, (req, res, next) => {
    res.json(req.user)
})


module.exports = router