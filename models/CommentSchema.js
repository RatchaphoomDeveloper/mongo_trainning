var mongoose = require("mongoose")

const CommentSchema = new mongoose.Schema({
    postId: {
        type: Number,
        required: false,
    },
    userId: {
        type: Number,
        required: false,
    },
    id: {
        type: Number,
        required: false,
    },
    name: {
        type: String,
        required: false,
    },
    email: {
        type: String,
        required: false,
    },
    body: {
        type: String,
        required: false,
    }, 
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {
        type: Date,
        default: Date.now
    }

})

module.exports = mongoose.model("Comments", CommentSchema, "Comments")